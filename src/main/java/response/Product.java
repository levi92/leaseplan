package response;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
public class Product {

    private String provider;

    private String title;

    private String url;

    private String brand;

    private String price;

    private String unit;

    private String isPromo;

    private String promoDetails;

    private String image;
}
