package starter.searchDemoAPI;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;

public class SearchDemoAPI {

    private String foodType;
    private final static String baseURI = "https://waarkoop-server.herokuapp.com/";
    private final static String basePath = "api/v1/search/demo/";

    public Response getProducts(String foodType) {

        return SerenityRest.given().baseUri(baseURI).basePath(basePath).given().get(foodType);
    }

}
