package starter.stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import static org.junit.Assert.*;
import net.thucydides.core.annotations.Steps;
import org.springframework.http.HttpStatus;
import response.Product;
import starter.searchDemoAPI.SearchDemoAPI;

import java.util.List;
import java.util.Objects;


public class SearchStepDefinitions {

    @Steps
    Product product;
    Response response;
    SearchDemoAPI searchDemoAPI = new SearchDemoAPI();
    List<Product> listOfProducts;

    @When("I search for {string}")
    public void iSearchFor(String foodType) {

        response = searchDemoAPI.getProducts(foodType);

        if(response.getStatusCode() == HttpStatus.OK.value())
            listOfProducts = response.getBody().jsonPath().getList("", Product.class);

    }

    @Then("I see the {string} response")
    public void iSeeResultsForTheProduct(String responseType) {

        assertEquals(responseType, Objects.requireNonNull(HttpStatus.resolve(response.getStatusCode())).getReasonPhrase());

    }

    @Then("I see {int} products in the response")
    public void iSeeNumberOfProducts(Integer exprectedNumberOfProducts){
        Integer actualNumberOfProducts = listOfProducts.size();
        assertEquals(exprectedNumberOfProducts, actualNumberOfProducts);
    }

    @Then("I see provider for the products is {string}")
    public void iSeeVendorOfProducts(String provider){
        for (Product product:listOfProducts) {
            assertEquals(provider, product.getProvider());
        }
    }


}
