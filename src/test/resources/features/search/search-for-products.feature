@searchForProducts
Feature: Search for the food products

### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/demo/{product} for getting the products.
### Available products: "orange", "apple", "pasta", "cola"
### Prepare Positive and negative scenarios

  @successfulSearch
  Scenario Outline: Successful search for different types of products
    When I search for <food-type>
    Then I see the <response> response
    And I see <number-of-products> products in the response
    And I see provider for the products is <provider>

    Examples:
      | food-type  | response | number-of-products | provider  |
      | "apple"    | "OK"     | 9                  | "Vomar"   |
      | "orange"   | "OK"     | 0                  | ""        |
      | "pasta"    | "OK"     | 20                 | "Coop"    |
      | "cola"     | "OK"     | 20                 | "Coop"    |

  @unsuccessfulSearch
  Scenario: Search for non existing products gives error response
    When I search for "car"
    Then I see the "Not Found" response
